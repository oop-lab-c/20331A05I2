#include<iostream>
using namespace std;
class animal{
    public:
    virtual void bark()=0;
};
class dog: public animal
{
    public:
    void bark()
    {
        cout<<"Bow Bow"<<endl;
    }
};
int main()
{
    dog obj;
    obj.bark();
}