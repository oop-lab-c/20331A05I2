import java.util.*;

interface sample {
    void show();

}

class PureAbsJava implements sample {
    public void show() {
        System.out.println("hello world ");
    }

    public static void main(String[] args) {
        PureAbsJava h = new PureAbsJava();
        h.show();

    }

}
