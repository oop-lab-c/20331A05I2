#include<iostream>
using namespace std;
class Person { //class Person
public:
    void person()
    {
        cout << "Iam a person" << endl;
    }
};
 
class Father : public Person { //class Father inherits Person
public:
    void father()
    {
        cout<<"father of person"<<endl;
    }
};
 
class Mother : public Person { //class Mother inherits Person
public:
    void mother()
    {
        cout<<"mother of person"<<endl;
    }
};
 
class Child : public Father, public Mother  { //Child inherits Father and Mother
public:
    void child()
    {
        cout<<"iam a child"<<endl;
    }
};
 
int main() {
    Child obj;
    obj.child();
}
