import java.lang.*;
import java.util.Scanner;

class EvenOdd {
	public void EvenOdd(int x)
	{
          if (x % 2 == 0)
            System.out.println("EVEN");
          else
            System.out.println("ODD");
	}
	public static void main(String[] args) {
     	   int a;
           Scanner input = new Scanner(System.in);
           System.out.println("Enter a num");
           a = input.nextInt();
	   EvenOdd obj = new EvenOdd();
	   obj.EvenOdd(a);
    }
}